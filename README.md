### TP REACT -> GIPHY API

* Mise en place de l'environnement de dev
	* npm
	* webpack
	* react
	* less
	* material design lite

* Integration Giphy API
  * http://api.giphy.com/v1/gifs/random?api_key=dc6zaTOxFJmzC


* Sujets de TP
	* Giphy Google Trends => difficulté 2

		* Utiliser ce json (http://hawttrends.appspot.com/api/terms/) et
	* Giphy Rebus Builder => difficulté 5++
	* Giphy Mosaic Builder => difficulté 10++
	* Giphy Memes Chrome extension => difficulté 5++
