'use strict';

var ROOT = process.env.PWD;
var webpack = require('webpack');
var path = require('path');
var nodeModulesDir = path.join(ROOT, 'node_modules');

var config = {
	devtool: 'cheap-module-eval-source-map',
	devServer: {
		contentBase: './build',
		port: 8080,
		host: 'localhost',
		hot: true,
		stats: {
			colors: true
		}
	},
	entry: {
		app: [
			'webpack/hot/dev-server',
			path.join(ROOT, './app/main.jsx')
		]
	},
	output: {
		path: path.join(ROOT, 'build'),
		filename: '[name].js',
		publicPath: '/'
	},
	resolve: {
		extensions: ['', '.js'],
		root: path.join(ROOT, 'app'),
		moduleDirectories: [nodeModulesDir]
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin()
	],
	module: {
		loaders: [
			{
				test: /\.(js|jsx)$/,
				loaders: ['react-hot', 'babel?stage=0'],
				exclude: nodeModulesDir
			},
			{
				test: /\.css$/,
				loader: 'style!css'
			},
			{
				test: /\.less$/,
				loader: 'style!css!less'
			}
		]
	}
};

module.exports = config;
