import React, { Component } from 'react';
import Giphy from './Giphy.js';
import testdata from './testdata.js';
import GiphyTrendItem from './GiphyTrendItem.jsx';

import "./trends.less";

export default class App extends Component {

	state = {
		trends: testdata
	}

	render(){
		return (
			<div className="giphy-trends">
				{this.state.trends['1'].map(trend => <GiphyTrendItem key={trend} trend={trend} />)}
			</div>
		);
	}
}
