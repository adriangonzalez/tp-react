const GIPHY_GIFS = 'http://api.giphy.com/v1/gifs/';
const GIPHY_STICKERS = 'http://api.giphy.com/v1/stickers/';
const GIPHY_DEV_API_KEY = 'dc6zaTOxFJmzC';

class Gifs {
	static random() {
		return fetch(`${GIPHY_GIFS}random?api_key=${GIPHY_DEV_API_KEY}`)
			.then(resp => resp.json())
			.catch(console.error);
	}
	static 	trending() {
		return fetch(`${GIPHY_GIFS}trending?api_key=${GIPHY_DEV_API_KEY}`)
			.then(resp => resp.json())
			.catch(console.error);
	}
	static search(query) {
		return fetch(`${GIPHY_GIFS}search?api_key=${GIPHY_DEV_API_KEY}&q=${query}`)
			.then(resp => resp.json())
			.catch(console.error);
	}
	static translate(query) {
		return fetch(`${GIPHY_GIFS}translate?api_key=${GIPHY_DEV_API_KEY}&s=${query}`)
			.then(resp => resp.json())
			.catch(console.error);
	}
}

class Stickers {
	static random() {
		return fetch(`${GIPHY_STICKERS}random?api_key=${GIPHY_DEV_API_KEY}`)
			.then(resp => resp.json())
			.catch(console.error);
	}
	static 	trending() {
		return fetch(`${GIPHY_STICKERS}trending?api_key=${GIPHY_DEV_API_KEY}`)
			.then(resp => resp.json())
			.catch(console.error);
	}
	static search(query) {
		return fetch(`${GIPHY_STICKERS}search?api_key=${GIPHY_DEV_API_KEY}&q=${query}`)
			.then(resp => resp.json())
			.catch(console.error);
	}
	static translate(query) {
		return fetch(`${GIPHY_STICKERS}translate?api_key=${GIPHY_DEV_API_KEY}&s=${query}`)
			.then(resp => resp.json())
			.catch(console.error);
	}
}

export default class Giphy {
	static gifs() {
		return Gifs;
	}
	static stickers() {
		return Stickers;
	}
}
