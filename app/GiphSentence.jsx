import React, { Component } from 'react';
import Giphy from './Giphy.js';
import GifWord from './GifWord.jsx';

export default class GiphSentence extends Component {

	state = {}

	componentDidMount() {
		this.props.expressions.map(expression => {
			Giphy.gifs().translate(expression).then(json => {
				this.setState({
					[expression]: json.data.images['original'].url
				});
			});
		});
	}

	render () {

		let gifWords = Object.keys(this.state).map(expression => {
			return <GifWord url={this.state[expression]} expression={expression} key={expression} />
		});

		return (
			<div>
				{gifWords}
			</div>
		);
	}
}
