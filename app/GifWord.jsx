import React, { Component } from 'react';

export default class GiphyTrendItem extends Component {

	state = {
		loading: false
	}

	static defaultProps = {
		url: ''
	}

	onLoadImg = () => {
		this.setState({
			loading: false
		});
	}

	render(){
		return (
			<div>
				<img src={this.props.url} onLoad={this.onLoadImg} height={100}/>
			</div>
		);
	}
}
