import React from 'react';
import { render } from 'react-dom';
import App from './App.jsx';
import RebusBuilder from './RebusBuilder.jsx';

import 'file?name=[name].[ext]!./index.html';

var root = render(<RebusBuilder />, document.getElementById('content'));

// Hot loading magic
require('react-hot-loader/Injection').RootInstanceProvider.injectProvider({
	getRootInstances: () => {
		return [root];
	}
});
