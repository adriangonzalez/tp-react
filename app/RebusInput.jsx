import React, { PropTypes } from 'react';

export default class RebusInput extends React.Component {

	onChange = (e) => {
		console.log(e);
	}

	render () {
		return (
			<input type='text' onChange={this.onChange} />
		);
	}
}
