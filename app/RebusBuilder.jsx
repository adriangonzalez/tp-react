import React, { Component } from 'react';
import RebusInput from './RebusInput.jsx';
import GiphSentence from './GiphSentence.jsx';

export default class RebusBuilder extends Component {
	render () {
		return (
			<div>
				<RebusInput />
				<GiphSentence expressions={['long', 'cat', 'is long']}/>
			</div>
		)
	}
}
