import React, { Component } from 'react';
import Giphy from './Giphy.js';

export default class GiphyTrendItem extends Component {

	state = {
		trend: '',
		url: '',
		fetching: false,
		loading: false
	}

	static defaultProps = {
		trend: ''
	}

	fetchGiphyTrend = (trend) => {
		this.setState({ fetching: true, trend: trend });
		Giphy.gifs().translate(trend).then(json => {
			console.log(json);
			this.setState({
				loading: true,
				fetching: false,
				url: json.data.images['original'].url
			});
		});
	}

	onLoadImg = () => {
		this.setState({
			loading: false
		});
	}

	componentDidMount() {
		console.log(this.props);
		this.fetchGiphyTrend(this.props.trend);
	}

	render(){
		return (
			<div>
				<img src={this.state.url} className="giphy-trend-item" onLoad={this.onLoadImg} height={100}/>
				<span>{this.props.trend}</span>
			</div>
		);
	}
}
